// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import VueResource from 'vue-resource'
import store from './store'
import Notifications from 'vue-notification'
import velocity from 'velocity-animate'
import ziConfig from './config.js'
import Auth from './packages/auth/Auth.js'
import vueHeadful from 'vue-headful'
import lodash from 'lodash'
import VueLodash from 'vue-lodash'
import VeeValidate from 'vee-validate'
import VueEvents from 'vue-events'
import VueSwal from 'vue-swal'

// Require Froala Editor js file.
require('froala-editor/js/froala_editor.pkgd.min')

// Require Froala Editor css files.
require('froala-editor/css/froala_editor.pkgd.min.css')
require('font-awesome/css/font-awesome.css')
require('froala-editor/css/froala_style.min.css')

// Import and use Vue Froala lib.
import VueFroala from 'vue-froala-wysiwyg'
import {
  Vuetify,
  VApp,
  VAlert,
  VNavigationDrawer,
  VFooter,
  VList,
  VBtn,
  VBtnToggle,
  VCarousel,
  VChip,
  VForm,
  VJumbotron,
  VIcon,
  VGrid,
  VRadioGroup,
  VDatePicker,
  VStepper,
  VToolbar,
  VAvatar,
  VDivider,
  VDialog,
  VCard,
  VMenu,
  VTooltip,
  VTabs,
  VSubheader,
  VCheckbox,
  VTextField,
  VProgressCircular,
  VProgressLinear,
  VExpansionPanel,
  VSelect,
  VSwitch,
  VSpeedDial,
  VSnackbar,
  VParallax,
  transitions
} from 'vuetify'
import '../node_modules/vuetify/src/stylus/app.styl'

Vue.use(VueResource)
Vue.use(Notifications, { velocity })
Vue.use(ziConfig)
Vue.use(Auth)
Vue.component('vue-headful', vueHeadful)
Vue.use(VueLodash, lodash)
Vue.use(VeeValidate)
Vue.use(VueEvents)
Vue.use(VueSwal)
Vue.use(VueFroala)

Vue.use(Vuetify, {
  components: {
    VApp,
    VAlert,
    VNavigationDrawer,
    VFooter,
    VList,
    VBtn,
    VBtnToggle,
    VCarousel,
    VChip,
    VForm,
    VJumbotron,
    VIcon,
    VGrid,
    VRadioGroup,
    VDatePicker,
    VStepper,
    VToolbar,
    VAvatar,
    VDivider,
    VDialog,
    VCard,
    VMenu,
    VTooltip,
    VTabs,
    VSubheader,
    VCheckbox,
    VTextField,
    VProgressCircular,
    VExpansionPanel,
    VProgressLinear,
    VSelect,
    VSwitch,
    VSpeedDial,
    VSnackbar,
    VParallax,
    transitions
  },
  theme: {
    primary: '#2196F3',
    secondary: '#113662',
    accent: '#82B1FF',
    error: '#FF5252',
    info: '#2196F3',
    success: '#4CAF50',
    warning: '#FFC107'
  }
})

Vue.http.options.root = Vue.ziConfig.apiDomain
Vue.http.headers.common['Authorization'] = 'Bearer ' + Vue.auth.getToken()
Vue.http.headers.common['Accept'] = 'application/json'
Vue.http.headers.common['Access-Control-Allow-Origin'] = Vue.ziConfig.domain

Vue.config.productionTip = false

router.beforeEach(
   (to, from, next) => {
     if (to.matched.some(record => record.meta.forVisitors)) {
       if (Vue.auth.isAuthenticated()) {
         next({
           path: '/' + store.state.userStore.authUser.username + '/home'
         })
       } else {
         next()
       }
     } else if (to.matched.some(record => record.meta.forAuth)) {
       if (!Vue.auth.isAuthenticated()) {
         next({
           path: '/login'
         })
       } else {
         next()
       }
     } else {
       next()
     }
   }
)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
