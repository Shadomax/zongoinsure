import Vue from 'vue'
import router from '../router'

const state = {
  authUser: {},
  userToken: {
    token: '',
    expiration: ''
  },
}

const getters = {}

const mutations = {
  Set_User_Token (state, payload) {
    state.userToken.token = payload.token
    state.userToken.expiration = payload.expiration
  },

  Set_Name (state, payload) {
    state.authUser.fullName = payload
  },

  Set_Phone_Number (state, payload) {
    state.authUser.phoneNumber = payload
  },

  Set_Username (state, payload) {
    state.authUser.username = payload
  },

  Set_Description (state, payload) {
    state.authUser.description = payload
  },

  Set_Authenticated_User (state, payload) {
    state.authUser = payload.data
  },

  CLEAR_AUTH_USER (state) {
    Vue.auth.destroyToken()
    state.authUser = null,
    state.userToken.token = null,
    state.userToken.expiration = null
  },
}

const actions = {
  signUp ({commit}, payload) {
    return Vue.http.post('account/register', payload)
     .then(response => {
      // success callback
       return response
     }, response => {
      // error callback
       return response
     })
  },

  verifyAccount ({commit}, payload) {
    return Vue.http.patch('account/verify/' + payload.code)
     .then(response => {
      // success callback
       return response
     }, response => {
      // error callback
       return response
     })
  },

  signIn ({commit}, payload) {
    return Vue.http.post('account/login', payload)
     .then(response => {
      // success callback
       Vue.auth.setToken(response.body.access_token, (response.body.expires_in * 1000) + Date.now())
       commit('Set_User_Token', {token: response.body.access_token, expiration: (response.body.expires_in * 1000) + Date.now()})
       return response
     }, response => {
      // error callback
       return response
     })
  },

  userForgotPassword ({commit}, payload) {
    let postData = {
      email: payload.email
    }
    return Vue.http.post('accounnt/forgot-password', postData)
     .then(response => {
      // success callback
       // commit anything if any
       return response
     }, response => {
      // error callback
       return response
     })
  },

  resetPassword ({commit}, payload) {
    let postData = {
      token: payload.token,
      password: payload.password,
      confirm_password: payload.confirm_password
    }
    return Vue.http.post('account/reset-password', postData)
     .then(response => {
       return response
     }, response => {
       return response
     })
  },

  setAuthenticatedUser ({commit, state, dispatch}) {
    if (Vue.auth.isAuthenticated()) {
      commit('Set_User_Token', {token: Vue.auth.getToken()})
      Vue.http.get('account/me')
      .then(response => {
        commit('Set_Authenticated_User', response.body)
      }, response => {
        Vue.auth.destroyToken()
        window.location.href = '/login'
      })
    } else {
      console.log('log out')
      Vue.auth.destroyToken()
      // window.location.href = '/login'
    }
  },

  changePassword ({commit, state, dispatch}, payload) {
    return Vue.http.patch('account/change_password', payload)
      .then(response => {
        dispatch('setAuthenticatedUser')
        return response
      }, response => {
        return response
      })
  },

  update ({commit, state, dispatch}, payload) {
    return Vue.http.patch('account/update', payload)
      .then(response => {
        dispatch('setAuthenticatedUser')
        return response
      }, response => {
        return response
      })
  },

  updateMedia ({commit, state, dispatch}, payload) {
    return Vue.http.post('account/update_media', payload)
      .then(response => {
        dispatch('setAuthenticatedUser')
        return response
      }, response => {
        return response
      })  
  },

  getStarted ({state, commit, dispatch}, payload) {
    return Vue.http.post('account/company/get-started/' + payload.user_id, payload)
      .then(response => {
        dispatch('setAuthenticatedUser')
        return response
      }, response => {
        return response
      })  
  },

  addCompanyAbout ({state, commit, dispatch}, payload) {
    return Vue.http.patch('account/company/add_about/' + payload.company_id, payload)
    .then(response => {
      dispatch('setAuthenticatedUser')
      return response
    }, response => {
      return response
    }) 
  },

  updateCompanyMedia ({state, commit, dispatch}, payload) {
    return Vue.http.post('account/company/update_media/' + payload.get('company_id'), payload)
    .then(response => {
      dispatch('setAuthenticatedUser')
      return response
    }, response => {
      return response
    }) 
  },

  addService ({state, commit, dispatch}, payload) {
    return Vue.http.post('account/company/add_service/' + payload.get('company_id'), payload)
    .then(response => {
      dispatch('setAuthenticatedUser')
      return response
    }, response => {
      return response
    }) 
  },

  addOffice ({state, commit, dispatch}, payload) {
    return Vue.http.patch('account/company/add_office/' + payload.company_id, payload)
    .then(response => {
      dispatch('setAuthenticatedUser')
      return response
    }, response => {
      return response
    }) 
  },

  addTeam ({state, commit, dispatch}, payload) {
    return Vue.http.post('account/company/add_team/' + payload.get('company_id'), payload)
    .then(response => {
      dispatch('setAuthenticatedUser')
      return response
    }, response => {
      return response
    }) 
  },

  clearAuthUser ({commit}) {
    return Vue.http.get('account/logout')
     .then(resp => {
       commit('CLEAR_AUTH_USER')
       window.location.href = '/login'
     }, resp => {
      return resp
     })
  }
}

export default {
  state, getters, mutations, actions
}
