export default function (Vue) {
  Vue.ziConfig = {
    domain: 'http://localhost:8080',
    apiDomain: 'http://localhost:8000/api/',
    APP_NAME: 'ZI'
  }

  Object.defineProperties(Vue.prototype, {
    $ziConfig: {
      get: () => {
        return Vue.ziConfig
      }
    }
  })
}
